﻿using ContosoUniversity.Commons;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace ContosoUniversity.IS
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources() =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
            };

        public static IEnumerable<ApiResource> GetApiResources() =>
            new List<ApiResource>
            {
                new ApiResource(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<ApiScope> GetApiScopes() =>
            new List<ApiScope>
            {
                 new ApiScope(Constants.ASSEMBLY_API)
            };

        public static IEnumerable<Client> GetClients() =>
            new List<Client>
            {
               new Client {
                    ClientId = Constants.CLIENT_ANGULAR,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    RedirectUris = { Constants.URL_ANGULAR },
                    PostLogoutRedirectUris = { Constants.URL_ANGULAR },
                    AllowedCorsOrigins = { Constants.URL_ANGULAR },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        Constants.ASSEMBLY_API,
                    },
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
               },
               new Client {
                    ClientId = Constants.CLIENT_RAZOR,
                    ClientSecrets = { new Secret(Constants.CLIENT_SECRETE.ToSha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RedirectUris = { Constants.URL_RAZOR + "/signin-oidc" },
                    PostLogoutRedirectUris = { Constants.URL_RAZOR + "/signout-callback-oidc" },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    AllowOfflineAccess = true,
                    RequireConsent = false,
               },
               new Client {
                    ClientId = Constants.CLIENT_BLAZOR,
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    AllowedCorsOrigins = { Constants.URL_BLAZOR },
                    AllowedScopes = { "openid", "profile", "email", Constants.ASSEMBLY_API },
                    RedirectUris = { Constants.URL_BLAZOR + "/authentication/login-callback" },
                    PostLogoutRedirectUris = { Constants.URL_BLAZOR + "/" },
                    Enabled = true
               },
            };
    }
}
