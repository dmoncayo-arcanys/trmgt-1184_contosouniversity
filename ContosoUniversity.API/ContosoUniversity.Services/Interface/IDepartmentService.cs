﻿using ContosoUniversity.Entities;
using ContosoUniversity.Services.Core;

namespace ContosoUniversity.Services.Interface
{
    public interface IDepartmentService : IActionManager<Department>
    {
    }
}
