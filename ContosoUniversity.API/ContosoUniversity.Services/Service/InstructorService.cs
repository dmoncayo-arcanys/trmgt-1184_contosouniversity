﻿using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Services.Service
{
    public class InstructorService : IInstructorService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<InstructorService> logger;

        /// <summary>
        /// Constructor.
        /// </summary
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public InstructorService(
            IUnitOfWork unitOfWork,
            IRepository repository,
            ILogger<InstructorService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Save changes.
        /// </summary>
        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Get instructor by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Instructor</returns>
        public Instructor Get(int? id)
        {
            Instructor instructor = null;
            try
            {
                instructor = repository.All<Instructor>()
                    .Include(i => i.OfficeAssignment)
                    .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                        .ThenInclude(i => i.Department)
                    .Include(i => i.CourseAssignments)
                        .ThenInclude(i => i.Course)
                            .ThenInclude(i => i.Enrollments)
                                .ThenInclude(i => i.Student)
                    .AsNoTracking()
                    .FirstOrDefault(m => m.ID == id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return instructor;
        }

        /// <summary>
        /// Get instructor.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Instructor> GetAll()
        {
            IEnumerable<Instructor> instructor = null;
            try
            {
                instructor = repository.All<Instructor>()
                    .Include(i => i.OfficeAssignment)
                    .Include(i => i.CourseAssignments)
                    .ThenInclude(i => i.Course)
                        .ThenInclude(i => i.Department)
                    .Include(i => i.CourseAssignments)
                        .ThenInclude(i => i.Course)
                            .ThenInclude(i => i.Enrollments)
                                .ThenInclude(i => i.Student)
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return instructor;
        }

        /// <summary>
        /// Update instructor record.
        /// </summary>
        /// <param name="instructor"></param>
        /// <returns>bool</returns>
        public bool Update(InstructorUpdate instructor)
        {
            bool flag = true;
            try
            {
                var find = repository.All<Instructor>().AsNoTracking().FirstOrDefault(m => m.ID == instructor.Instructors.ID);
                if (find != null)
                {
                    var results = repository.All<Instructor>()
                        .Include(i => i.CourseAssignments)
                        .AsNoTracking()
                        .FirstOrDefault(m => m.ID == instructor.Instructors.ID);
                    instructor.Instructors.CourseAssignments = new List<CourseAssignment>();
                    if (instructor.SelectedCourses != null)
                    {
                        instructor.Instructors.CourseAssignments = results.CourseAssignments;
                        instructor.Instructors.OfficeAssignment = results.OfficeAssignment;
                        var selectedCoursesHS = new HashSet<string>(instructor.SelectedCourses);
                        var instructorCourses = new HashSet<int>(instructor.Instructors.CourseAssignments.Select(c => c.CourseID));
                        foreach (var course in repository.All<Course>())
                        {
                            if (selectedCoursesHS.Contains(course.CourseID.ToString()))
                            {
                                if (!instructorCourses.Contains(course.CourseID))
                                {
                                    repository.Create(new CourseAssignment
                                    {
                                        InstructorID = instructor.Instructors.ID,
                                        CourseID = course.CourseID
                                    });
                                }
                            }
                            else
                            {
                                if (instructorCourses.Contains(course.CourseID))
                                {
                                    CourseAssignment courseToRemove = instructor.Instructors.CourseAssignments
                                        .SingleOrDefault(i => i.CourseID == course.CourseID);
                                    courseToRemove.Instructor = null;
                                    repository.Delete(courseToRemove);
                                }
                            }
                        }
                    }
                    repository.Update(instructor.Instructors);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Delete instructor record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var instructor = repository.All<Instructor>().AsNoTracking().FirstOrDefault(m => m.ID == id);
                if (instructor != null)
                {
                    repository.Delete(instructor);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="instructor"></param>
        public bool Insert(Instructor instructor)
        {
            bool flag = true;
            try
            {
                if (instructor != null)
                {
                    repository.Create(instructor);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        public bool Update(Instructor entity)
        {
            throw new NotImplementedException();
        }
    }
}
