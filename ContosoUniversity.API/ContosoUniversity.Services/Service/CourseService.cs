﻿using ContosoUniversity.Entities;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Services.Service
{
    public class CourseService : ICourseService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<CourseService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public CourseService(IUnitOfWork unitOfWork, IRepository repository, ILogger<CourseService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Save changes.
        /// </summary>
        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Get course by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Course</returns>
        public Course Get(int? id)
        {
            Course course = null;
            try
            {
                course = repository.All<Course>()
                    .Include(s => s.Department)
                    .Include(s => s.Department.Administrator)
                    .Include(s => s.CourseAssignments)
                    .AsNoTracking()
                    .FirstOrDefault(m => m.CourseID == id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Get course.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Course> GetAll()
        {
            IEnumerable<Course> course = null;
            try
            {
                course = repository.All<Course>()
                    .Include(s => s.Department)
                    .Include(s => s.Department.Administrator)
                    .Include(s => s.CourseAssignments)
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return course;
        }

        /// <summary>
        /// Update course record.
        /// </summary>
        /// <param name="course"></param>
        /// <returns>bool</returns>
        public bool Update(Course course)
        {
            bool flag = true;
            try
            {
                var find = repository.All<Course>().AsNoTracking().FirstOrDefault(m => m.CourseID == course.CourseID);
                if (find != null)
                {
                    repository.Update(course);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Delete course record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var course = repository.All<Course>().AsNoTracking().FirstOrDefault(m => m.CourseID == id);
                if (course != null)
                {
                    repository.Delete(course);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="student"></param>
        public bool Insert(Course course)
        {
            bool flag = true;
            try
            {
                if (course != null)
                {
                    repository.Create(course);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
