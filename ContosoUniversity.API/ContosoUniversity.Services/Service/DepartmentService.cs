﻿using ContosoUniversity.Entities;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ContosoUniversity.Services.Service
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository repository;
        private readonly ILogger<DepartmentService> logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public DepartmentService(IUnitOfWork unitOfWork, IRepository repository, ILogger<DepartmentService> logger)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.logger = logger;
        }

        public IUnitOfWork UnitOfWork => unitOfWork;

        /// <summary>
        /// Save changes.
        /// </summary>
        public void SaveChanges()
        {
            UnitOfWork.SaveChanges();
        }

        /// <summary>
        /// Get department by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Course</returns>
        public Department Get(int? id)
        {
            Department department = null;
            try
            {
                department = repository.All<Department>()
                    .Include(s => s.Administrator)
                    .AsNoTracking()
                    .FirstOrDefault(m => m.DepartmentID == id);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return department;
        }

        /// <summary>
        /// Get course.
        /// </summary>
        /// <returns>IEnumerable<Instructor></returns>
        public IEnumerable<Department> GetAll()
        {
            IEnumerable<Department> department = null;
            try
            {
                department = repository.All<Department>()
                    .Include(s => s.Administrator)
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
            }
            return department;
        }

        /// <summary>
        /// Update department record.
        /// </summary>
        /// <param name="department"></param>
        /// <returns>bool</returns>
        public bool Update(Department department)
        {
            bool flag = true;
            try
            {
                var find = repository.All<Department>().AsNoTracking().FirstOrDefault(m => m.DepartmentID == department.DepartmentID);
                if (find != null)
                {
                    repository.Update(department);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Delete course record.
        /// </summary>
        /// <param name="id"></param>
        public bool Delete(int? id)
        {
            bool flag = true;
            try
            {
                var department = repository.All<Department>().AsNoTracking().FirstOrDefault(m => m.DepartmentID == id);
                if (department != null)
                {
                    repository.Delete(department);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }

        /// <summary>
        /// Insert new record.
        /// </summary>
        /// <param name="department"></param>
        public bool Insert(Department department)
        {
            bool flag = true;
            try
            {
                if (department != null)
                {
                    repository.Create(department);
                    SaveChanges();
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                flag = false;
            }
            return flag;
        }
    }
}
