using ContosoUniversity.Entities;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace ContosoUniversity.UnitTest
{
    public class DepartmentServiceTest
    {
        private readonly DepartmentService departmentService;
        private readonly Mock<IUnitOfWork> unitOfWork = new();
        private readonly Mock<IRepository> repository = new();
        private readonly Mock<ILogger<DepartmentService>> logger = new();

        public DepartmentServiceTest()
        {
            departmentService = new DepartmentService(unitOfWork.Object, repository.Object, logger.Object);
        }

        [Fact]
        public void Insert_ShouldReturnTrue_WhenDataIsValid()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            // Act
            var results = departmentService.Insert(department);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Insert_ShouldReturnFalse_WhenDataIsNotValid()
        {
            // Act
            var results = departmentService.Insert(null);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataExist()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);

            // Act
            var results = departmentService.GetAll();

            // Assert
            Assert.NotNull(results);
        }

        [Fact]
        public void GetAll_ShouldReturnValue_WhenDataNotExist()
        {
            // Arrange
            IQueryable<Department> query = null;

            repository.Setup(x => x.All<Department>()).Returns(query);

            // Act
            var results = departmentService.GetAll();

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnValue_WhenDataIsFound()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);

            // Act
            var results = departmentService.Get(departmentId);

            // Assert
            Assert.Equal(departmentId, results.DepartmentID);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenDataNotFound()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = 2,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);

            // Act
            var results = departmentService.Get(departmentId);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Get_ShouldReturnNull_WhenParameterIsInvalid()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);

            // Act
            var results = departmentService.Get(null); // Parameter is null.

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void Delete_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Delete(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Delete(departmentId);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Delete(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Delete(2); // Different from 'courseId'

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Delete_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Delete(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Delete(null); // Parameter is null.

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnTrue_WhenItemExist()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Update(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Update(department);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenItemNotExist()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            var departmentUpdate = new Department
            {
                DepartmentID = 2,
                Budget = 12000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Update(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Update(departmentUpdate);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void Update_ShouldReturnFalse_WhenParameterIsInvalid()
        {
            // Arrange
            var departmentId = 1;

            var department = new Department
            {
                DepartmentID = departmentId,
                Budget = 10000,
                StartDate = DateTime.Now,
                InstructorID = 1,
            };

            IQueryable<Department> query = Enumerable.Empty<Department>().AsQueryable();
            query = query.Concat(new Department[] { department });

            repository.Setup(x => x.All<Department>()).Returns(query);
            repository.Setup(x => x.Update(department));
            unitOfWork.Setup(x => x.SaveChanges());

            // Act
            var results = departmentService.Update(null);

            // Assert
            Assert.False(results);
        }
    }
}
