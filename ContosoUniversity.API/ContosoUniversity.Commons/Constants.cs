﻿namespace ContosoUniversity.Commons
{
    public class Constants
    {
        // URL
        public const string URL_IDENTITY_SERVER = "https://localhost:44313/";
        public const string URL_ANGULAR = "http://localhost:4200";
        public const string URL_RAZOR = "https://localhost:44361";
        public const string URL_BLAZOR = "https://localhost:44332";
        public const string URL_API = "https://localhost:44350/";

        // Config
        public const string VERSION = "v1";
        public const string CLIENT_ANGULAR = "client_angular";
        public const string CLIENT_RAZOR = "client_razor";
        public const string CLIENT_BLAZOR = "client_blazor";
        public const string CLIENT_SECRETE = "client_secret_razor";

        // DB
        public const string DB_CONTEXT = "DbContext";
        public const string ASSEMBLY_API = "ContosoUniversity.API";

        // API
        public const string STUDENTS = "Students";
        public const string COURSES = "Courses";
        public const string DEPARTMENTS = "Department";
        public const string INSTRUCTORS = "Instructors";

        // Pagination
        public const int PAGE_CONTENT = 5;

        // Filter
        public const string FILTER_NAME_DESC = "name_desc";
        public const string FILTER_DATE = "date";
        public const string FILTER_DATE_DESC = "date_desc";

        // Format
        public const string FORMAT_APP_JSON = "application/json";
    }
}
