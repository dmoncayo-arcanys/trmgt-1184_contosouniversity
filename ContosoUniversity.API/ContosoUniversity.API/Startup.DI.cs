﻿using ContosoUniversity.Repositories;
using ContosoUniversity.Repositories.Interface;
using ContosoUniversity.Repositories.Repository;
using ContosoUniversity.Services.Interface;
using ContosoUniversity.Services.Service;
using Microsoft.Extensions.DependencyInjection;

namespace ContosoUniversity.API
{
    public partial class Startup
    {
        private static void ConfigureDI(IServiceCollection services)
        {
            services.AddScoped<IDbFactory, DbFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IInstructorService, InstructorService>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IDepartmentService, DepartmentService>();

            services.AddScoped<IRepository, Repository>();
        }
    }
}
