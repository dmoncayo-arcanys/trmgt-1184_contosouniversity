﻿using ContosoUniversity.Entities;
using ContosoUniversity.Models;
using ContosoUniversity.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseService courseService;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="courseService"></param>
        public CoursesController(ICourseService courseService)
        {
            this.courseService = courseService;
        }

        /// <summary>
        /// GET: /Courses
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetCourses()
        {
            ApiResultModel results = new();
            results.Response = courseService.GetAll();
            results.Status = results.Response != null? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// GET: api/Courses/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpGet("{id}")]
        public IActionResult GetCourse(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = courseService.Get(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// POST: api/Courses
        /// </summary>
        /// <param name="course"></param>
        /// <returns>IActionResult</returns>
        [HttpPost]
        public IActionResult PostCourses(Course course)
        {
            ApiResultModel results = new();
            results.Response = courseService.Insert(course);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// PUT: api/Courses/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="course"></param>
        /// <returns>IActionResult</returns>
        [HttpPut("{id}")]
        public IActionResult PutCourse(int? id, Course course)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = courseService.Update(course);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }

        /// <summary>
        /// DELETE: api/Courses/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IActionResult</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteCourse(int? id)
        {
            ApiResultModel results = new();
            if (id == null)
            {
                results.Status = Status.Error;
                return Ok(results);
            }
            results.Response = courseService.Delete(id);
            results.Status = results.Response != null ? Status.Success : Status.Error;
            return Ok(results);
        }
    }
}
