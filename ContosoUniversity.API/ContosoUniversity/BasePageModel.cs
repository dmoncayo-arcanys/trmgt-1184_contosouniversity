﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ContosoUniversity
{
    public class BasePageModel : PageModel
    {
        public IHttpClientFactory httpClientFactory;

        public BasePageModel(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public async Task<ApiResultModel> GetAsync(string api, int? id = null)
        {
            // Refresh token on get call.
            await RefreshAccessToken();

            ApiResultModel value = new();

            var client = await GetHttpClientAsync();
            var responseTask = id == null ? client.GetAsync(api) : client.GetAsync(api + "/" + id);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                string readTask = await result.Content.ReadAsStringAsync();
                value = JsonConvert.DeserializeObject<ApiResultModel>(readTask);
            }

            value.StatusCode = result.StatusCode;
            return value;
        }

        public async Task<HttpClient> GetHttpClientAsync()
        {
            var accessToken = await GetTokenAsync();
            var client = httpClientFactory.CreateClient(Constants.CLIENT_RAZOR);
            client.SetBearerToken(accessToken);
            return client;
        }

        public async Task<string> GetTokenAsync()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }

        public HttpContent GetHttpContent(string value)
        {
            return new StringContent(value, Encoding.UTF8, Constants.FORMAT_APP_JSON);
        }

        public JArray FormatList(ApiResultModel value)
        { 
            return (JArray)value.Response;
        }

        public JObject Format(ApiResultModel value)
        {
            return (JObject)value.Response;
        }

        public async Task RefreshAccessToken()
        {
            var serverClient = httpClientFactory.CreateClient();
            var discoveryDocument = await serverClient.GetDiscoveryDocumentAsync(Constants.URL_IDENTITY_SERVER);
            var refreshToken = await HttpContext.GetTokenAsync("refresh_token");
            if (refreshToken != null && refreshToken != string.Empty)
            {
                var refreshTokenClient = httpClientFactory.CreateClient();

                var tokenResponse = await refreshTokenClient.RequestRefreshTokenAsync(
                    new RefreshTokenRequest
                    {
                        Address = discoveryDocument.TokenEndpoint,
                        RefreshToken = refreshToken,
                        ClientId = Constants.CLIENT_RAZOR,
                        ClientSecret = Constants.CLIENT_SECRETE
                    });

                var authInfo = await HttpContext.AuthenticateAsync("Cookies");
                authInfo.Properties.UpdateTokenValue("access_token", tokenResponse.AccessToken);
                authInfo.Properties.UpdateTokenValue("id_token", tokenResponse.IdentityToken);
                authInfo.Properties.UpdateTokenValue("refresh_token", tokenResponse.RefreshToken);
                await HttpContext.SignInAsync("Cookies", authInfo.Principal, authInfo.Properties);
            }
        }
    }
}
