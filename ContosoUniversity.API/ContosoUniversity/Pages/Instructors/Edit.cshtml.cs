﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Instructors
{
    [Authorize]
    public class EditModel : BasePageModel
    {
        [BindProperty]
        public Instructor Instructor { get; set; }
        public string ErrorMessage { get; set; }
        public List<AssignedCourseData> AssignedCourseDataList;

        public EditModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.INSTRUCTORS, id);

            if (results.Response != null)
            {
                Instructor = Format(results).ToObject<Instructor>();
            }
            else
            {
                return NotFound();
            }

            IEnumerable<Course> allCourses = Enumerable.Empty<Course>();
            results = await GetAsync(Constants.COURSES);
            if (results.Response != null)
            {
                allCourses = FormatList(results).ToObject<IEnumerable<Course>>();
                var instructorCourses = new HashSet<int>(Instructor.CourseAssignments.Select(c => c.CourseID));
                AssignedCourseDataList = new List<AssignedCourseData>();
                foreach (var course in allCourses)
                {
                    AssignedCourseDataList.Add(new AssignedCourseData
                    {
                        CourseID = course.CourseID,
                        Title = course.Title,
                        Assigned = instructorCourses.Contains(course.CourseID)
                    });
                }
            }

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(string[] selectedCourses)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            InstructorUpdate update = new();
            update.Instructors = Instructor;
            update.SelectedCourses = selectedCourses;

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(update));
            var responseTask = client.PutAsync(Constants.INSTRUCTORS + "/" + Instructor.ID, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_007;

            return Page();
        }
    }
}
