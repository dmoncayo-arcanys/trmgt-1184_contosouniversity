﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using ContosoUniversity.Entities.ViewModels;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Instructors
{
    [Authorize]
    public class IndexModel : BasePageModel
    {
        public InstructorIndexData Instructor { get; set; }
        public int InstructorID { get; set; }
        public int CourseID { get; set; }
        public string ErrorMessage { get; set; }

        public IndexModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task OnGetAsync(int? id, int? courseID)
        {
            Instructor = new InstructorIndexData();
            Instructor.Instructors = Enumerable.Empty<Instructor>();

            var results = await GetAsync(Constants.INSTRUCTORS);

            if (results.Response != null)
            {
                Instructor.Instructors = FormatList(results).ToObject<IEnumerable<Instructor>>();
                ErrorMessage = string.Empty;
            }

            if (results.StatusCode == HttpStatusCode.Unauthorized)
            {
                ErrorMessage = Messages.ERROR_000;
            }

            if (id != null)
            {
                InstructorID = id.Value;
                Instructor instructor = Instructor.Instructors.Single(i => i.ID == id.Value);
                Instructor.Courses = instructor.CourseAssignments.Select(s => s.Course);
            }

            if (courseID != null)
            {
                CourseID = courseID.Value;
                Instructor.Enrollments = Instructor.Courses.Single(x => x.CourseID == courseID).Enrollments;
            }
        }
    }
}
