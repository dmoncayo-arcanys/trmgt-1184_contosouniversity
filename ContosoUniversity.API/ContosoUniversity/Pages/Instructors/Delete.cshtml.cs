﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Instructors
{
    [Authorize]
    public class DeleteModel : BasePageModel
    {
        [BindProperty]
        public Instructor Instructor { get; set; }
        public string ErrorMessage { get; set; }

        public DeleteModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.INSTRUCTORS, id);

            if (results.Response != null)
            {
                Instructor = Format(results).ToObject<Instructor>();
            }
            else
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_002;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await GetHttpClientAsync();
            var responseTask = client.DeleteAsync(Constants.INSTRUCTORS + "/" + id);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            return RedirectToAction("./Delete", new { id, saveChangesError = true });
        }
    }
}
