﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Instructors
{
    [Authorize]
    public class DetailsModel : BasePageModel
    {
        public Instructor Instructor { get; set; }

        public DetailsModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.INSTRUCTORS, id);

            if (results.Response != null)
            {
                Instructor = Format(results).ToObject<Instructor>();
            }
            else
            {
                return NotFound();
            }
            return Page();
        }
    }
}
