﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;

namespace ContosoUniversity.Pages.Instructors
{
    [Authorize]
    public class CreateModel : BasePageModel
    {
        [BindProperty]
        public Instructor Instructor { get; set; }
        public string ErrorMessage { get; set; }

        public CreateModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async System.Threading.Tasks.Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Instructor));
            var responseTask = client.PostAsync(Constants.INSTRUCTORS, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_011;

            return Page();
        }
    }
}
