﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Students
{
    [Authorize]
    public class IndexModel : BasePageModel
    {
        public PaginatedList<Student> Student { get; set; }
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public string ErrorMessage { get; set; }

        public IndexModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;
            DateSort = sortOrder == Constants.FILTER_DATE ? Constants.FILTER_DATE_DESC : Constants.FILTER_DATE;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Student> studentIQ = Enumerable.Empty<Student>();

            var results = await GetAsync(Constants.STUDENTS);

            if (results.Response != null)
            {
                studentIQ = FormatList(results).ToObject<IEnumerable<Student>>();
                ErrorMessage = string.Empty;
            }

            if (results.StatusCode == HttpStatusCode.Unauthorized)
            {
                ErrorMessage = Messages.ERROR_000;
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                studentIQ = studentIQ.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstMidName.Contains(searchString));
            }

            studentIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => studentIQ.OrderByDescending(s => s.LastName),
                Constants.FILTER_DATE => studentIQ.OrderBy(s => s.EnrollmentDate),
                Constants.FILTER_DATE_DESC => studentIQ.OrderByDescending(s => s.EnrollmentDate),
                _ => studentIQ.OrderBy(s => s.LastName),
            };

            Student = PaginatedList<Student>.Create(studentIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
