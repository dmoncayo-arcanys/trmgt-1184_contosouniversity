﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Students
{
    [Authorize]
    public class DetailsModel : BasePageModel
    {
        public Student Student { get; set; }

        public DetailsModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.STUDENTS, id);

            if (results.Response != null)
            {
                Student = Format(results).ToObject<Student>();
            }
            else
            {
                return NotFound();
            }
            return Page();
        }
    }
}
