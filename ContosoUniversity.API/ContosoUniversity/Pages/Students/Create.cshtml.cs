﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;

namespace ContosoUniversity.Pages.Students
{
    [Authorize]
    public class CreateModel : BasePageModel
    {
        [BindProperty]
        public Student Student { get; set; }
        public string ErrorMessage { get; set; }

        public CreateModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async System.Threading.Tasks.Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Student));
            var responseTask = client.PostAsync(Constants.STUDENTS, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_010;

            return Page();
        }
    }
}
