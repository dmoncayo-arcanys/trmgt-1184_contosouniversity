﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages
{
    public class IndexModel : BasePageModel
    {
        [BindProperty]
        public string AuthButtonLabel { get; set; }

        public IndexModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {

        }

        public void OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                AuthButtonLabel = "Logout";
            }
            else
            {
                AuthButtonLabel = "Login";
            }
        }

        public IActionResult OnPost()
        {
            if (User.Identity.IsAuthenticated)
            {
                return new SignOutResult(new[] { "oidc", "Cookies" });
            }
            else
            {
                return RedirectToPage("./Login");
            }
        }
    }
}
