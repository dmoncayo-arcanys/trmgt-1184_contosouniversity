﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    [Authorize]
    public class EditModel : DepartmentNamePageModel
    {
        [BindProperty]
        public Course Course { get; set; }
        public string ErrorMessage { get; set; }

        public EditModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.COURSES, id);

            if (results.Response != null)
            {
                Course = Format(results).ToObject<Course>();
            }
            else
            {
                return NotFound();
            }

            await PopulateDepartmentsDropDownListAsync();
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Course));
            var responseTask = client.PutAsync(Constants.COURSES + "/" + Course.CourseID, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_005;

            return Page();
        }
    }
}
