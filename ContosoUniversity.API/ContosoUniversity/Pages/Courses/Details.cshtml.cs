﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    [Authorize]
    public class DetailsModel : BasePageModel
    {
        public Course Course { get; set; }

        public DetailsModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.COURSES, id);

            if (results.Response != null)
            {
                Course = Format(results).ToObject<Course>();
            }
            else
            {
                return NotFound();
            }
            return Page();
        }
    }
}
