﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    [Authorize]
    public class CreateModel : DepartmentNamePageModel
    {
        [BindProperty]
        public Course Course { get; set; }
        public string ErrorMessage { get; set; }

        public CreateModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await PopulateDepartmentsDropDownListAsync();
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Course));
            var responseTask = client.PostAsync(Constants.COURSES, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_012;

            return Page();
        }
    }
}
