﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    [Authorize]
    public class IndexModel : BasePageModel
    {
        public PaginatedList<Course> Course { get; set; }
        public string NameSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public string ErrorMessage { get; set; }

        public IndexModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Course> courseIQ = Enumerable.Empty<Course>();

            var results = await GetAsync(Constants.COURSES);

            if (results.Response != null)
            {
                courseIQ = FormatList(results).ToObject<IEnumerable<Course>>();
                ErrorMessage = string.Empty;
            }

            if (results.StatusCode == HttpStatusCode.Unauthorized)
            {
                ErrorMessage = Messages.ERROR_000;
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                courseIQ = courseIQ.Where(s => s.Title.Contains(searchString));
            }

            courseIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => courseIQ.OrderByDescending(s => s.Title),
                _ => courseIQ.OrderBy(s => s.Title),
            };

            Course = PaginatedList<Course>.Create(courseIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
