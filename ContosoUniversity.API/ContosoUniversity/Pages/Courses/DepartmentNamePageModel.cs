﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    public class DepartmentNamePageModel : BasePageModel
    {
        public SelectList DepartmentNameSL { get; set; }

        public DepartmentNamePageModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task PopulateDepartmentsDropDownListAsync( object selectedDepartment = null)
        {
            IEnumerable<Department> departmentsQuery = Enumerable.Empty<Department>();

            var results = await GetAsync(Constants.DEPARTMENTS);

            if (results.Response != null)
            {
                departmentsQuery = FormatList(results).ToObject<IEnumerable<Department>>();
                departmentsQuery = departmentsQuery.OrderBy(i => i.Name);
            }

            DepartmentNameSL = new SelectList(departmentsQuery, "DepartmentID", "Name", selectedDepartment);
        }
    }
}
