﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Courses
{
    [Authorize]
    public class DeleteModel : BasePageModel
    {
        [BindProperty]
        public Course Course { get; set; }
        public string ErrorMessage { get; set; }

        public DeleteModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.COURSES, id);

            if (results.Response != null)
            {
                Course = Format(results).ToObject<Course>();
            }
            else
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_003;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await GetHttpClientAsync();
            var responseTask = client.DeleteAsync(Constants.COURSES + "/" + id);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            return RedirectToAction("./Delete", new { id, saveChangesError = true });
        }
    }
}
