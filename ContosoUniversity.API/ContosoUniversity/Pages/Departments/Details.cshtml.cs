﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    [Authorize]
    public class DetailsModel : BasePageModel
    {
        public Department Department { get; set; }

        public DetailsModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.DEPARTMENTS, id);

            if (results.Response != null)
            {
                Department = Format(results).ToObject<Department>();
            }
            else
            {
                return NotFound();
            }
            return Page();
        }
    }
}
