﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    [Authorize]
    public class DeleteModel : BasePageModel
    {
        [BindProperty]
        public Department Department { get; set; }
        public string ErrorMessage { get; set; }

        public DeleteModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.DEPARTMENTS, id);

            if (results.Response != null)
            {
                Department = Format(results).ToObject<Department>();
            }
            else
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = Messages.ERROR_004;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await GetHttpClientAsync();
            var responseTask = client.DeleteAsync(Constants.DEPARTMENTS + "/" + id);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            return RedirectToAction("./Delete", new { id, saveChangesError = true });
        }
    }
}
