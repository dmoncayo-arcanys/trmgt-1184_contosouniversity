﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    [Authorize]
    public class CreateModel : InstructorNamePageModel
    {
        [BindProperty]
        public Department Department { get; set; }
        public string ErrorMessage { get; set; }

        public CreateModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await PopulateIntructorsDropDownListAsync();
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Department));
            var responseTask = client.PostAsync(Constants.DEPARTMENTS, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_012;

            return Page();
        }
    }
}
