﻿using ContosoUniversity.Commons;
using ContosoUniversity.Commons.Utils;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    [Authorize]
    public class IndexModel : BasePageModel
    {
        public PaginatedList<Department> Department { get; set; }
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }
        public string ErrorMessage { get; set; }

        public IndexModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = string.IsNullOrEmpty(sortOrder) ? Constants.FILTER_NAME_DESC : string.Empty;
            DateSort = sortOrder == Constants.FILTER_DATE ? Constants.FILTER_DATE_DESC : Constants.FILTER_DATE;

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IEnumerable<Department> departmentIQ = Enumerable.Empty<Department>();

            var results = await GetAsync(Constants.DEPARTMENTS);

            if (results.Response != null)
            {
                departmentIQ = FormatList(results).ToObject<IEnumerable<Department>>();
                ErrorMessage = string.Empty;
            }

            if (results.StatusCode == HttpStatusCode.Unauthorized)
            {
                ErrorMessage = Messages.ERROR_000;
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                departmentIQ = departmentIQ.Where(s => s.Name.Contains(searchString));
            }

            departmentIQ = sortOrder switch
            {
                Constants.FILTER_NAME_DESC => departmentIQ.OrderByDescending(s => s.Name),
                Constants.FILTER_DATE => departmentIQ.OrderBy(s => s.StartDate),
                Constants.FILTER_DATE_DESC => departmentIQ.OrderByDescending(s => s.StartDate),
                _ => departmentIQ.OrderBy(s => s.Name),
            };

            Department = PaginatedList<Department>.Create(departmentIQ, pageIndex ?? 1, Constants.PAGE_CONTENT);
        }
    }
}
