﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    public class InstructorNamePageModel : BasePageModel
    {
        public SelectList InstructorNameSL { get; set; }

        public InstructorNamePageModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task PopulateIntructorsDropDownListAsync(object selectedInstructor = null)
        {
            IEnumerable<Instructor> instructorService = Enumerable.Empty<Instructor>();

            var results = await GetAsync(Constants.INSTRUCTORS);

            if (results.Response != null)
            {
                instructorService = FormatList(results).ToObject<IEnumerable<Instructor>>();
                instructorService = instructorService.OrderBy(i => i.FullName);
            }

            InstructorNameSL = new SelectList(instructorService, "ID", "FullName", selectedInstructor);
        }
    }
}
