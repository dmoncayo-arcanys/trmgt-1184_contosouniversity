﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ContosoUniversity.Pages.Departments
{
    [Authorize]
    public class EditModel : InstructorNamePageModel
    {
        [BindProperty]
        public Department Department { get; set; }
        public string ErrorMessage { get; set; }

        public EditModel(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var results = await GetAsync(Constants.DEPARTMENTS, id);

            if (results.Response != null)
            {
                Department = Format(results).ToObject<Department>();
            }
            else
            {
                return NotFound();
            }

            await PopulateIntructorsDropDownListAsync();
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var client = await GetHttpClientAsync();
            var httpContent = GetHttpContent(JsonSerializer.Serialize(Department));
            var responseTask = client.PutAsync(Constants.DEPARTMENTS + "/" + Department.DepartmentID, httpContent);
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToPage("./Index");
            }

            ErrorMessage = Messages.ERROR_009;

            return Page();
        }
    }
}
