using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages
{
    [Authorize]
    public class LoginModel : PageModel
    {
        public RedirectToPageResult OnGet()
        {
            return RedirectToPage("./Index");
        }
    }
}
