﻿using ContosoUniversity.Commons;
using ContosoUniversity.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ContosoUniversity.Blazor
{
    public class CommonQuery
    {
        public static async Task<IEnumerable<Department>> GetDepartments(IHttpClientFactory httpClientFactory)
        {
            var departments = Enumerable.Empty<Department>();
            var results = await RequestHelper.GetAsync(httpClientFactory, Constants.DEPARTMENTS);
            if (results.Response != null)
            {
                departments = RequestHelper.FormatList(results).ToObject<IEnumerable<Department>>();
                departments = departments.OrderBy(i => i.Name);
            }
            return departments;
        }

        public static async Task<IEnumerable<Instructor>> GetInstructors(IHttpClientFactory httpClientFactory)
        {
            var departments = Enumerable.Empty<Instructor>();
            var results = await RequestHelper.GetAsync(httpClientFactory, Constants.INSTRUCTORS);
            if (results.Response != null)
            {
                departments = RequestHelper.FormatList(results).ToObject<IEnumerable<Instructor>>();
                departments = departments.OrderBy(i => i.FullName);
            }
            return departments;
        }
    }
}
