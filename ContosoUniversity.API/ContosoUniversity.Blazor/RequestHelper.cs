﻿using ContosoUniversity.Commons;
using ContosoUniversity.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ContosoUniversity.Blazor
{
    public class RequestHelper
    {
        public static async Task<ApiResultModel> GetAsync(IHttpClientFactory httpClientFactory, string api, string id = null)
        {
            var client = GetHttpClient(httpClientFactory);
            var result = id == null ? await client.GetAsync(api) : await client.GetAsync(api + "/" + id);
            return await FormatReturnValueAsync(result);
        }

        public static async Task<ApiResultModel> PostAsync(IHttpClientFactory httpClientFactory, string jsonSerializer, string api)
        {
            var client = GetHttpClient(httpClientFactory);
            var httpContent = GetHttpContent(jsonSerializer);
            var result = await client.PostAsync(api, httpContent);
            return await FormatReturnValueAsync(result);
        }

        public static async Task<ApiResultModel> PutAsync(IHttpClientFactory httpClientFactory, string jsonSerializer, string api)
        {
            var client = GetHttpClient(httpClientFactory);
            var httpContent = GetHttpContent(jsonSerializer);
            var result = await client.PutAsync(api, httpContent);
            return await FormatReturnValueAsync(result);
        }

        public static async Task<ApiResultModel> DeleteAsync(IHttpClientFactory httpClientFactory, string api)
        {
            var client = GetHttpClient(httpClientFactory);
            var result = await client.DeleteAsync(api);
            return await FormatReturnValueAsync(result);
        }

        private static async Task<ApiResultModel> FormatReturnValueAsync(HttpResponseMessage result)
        {
            ApiResultModel value = new();
            if (result.IsSuccessStatusCode)
            {
                string readTask = await result.Content.ReadAsStringAsync();
                value = JsonConvert.DeserializeObject<ApiResultModel>(readTask);
            }
            value.StatusCode = result.StatusCode;
            return value;
        }

        public static HttpClient GetHttpClient(IHttpClientFactory httpClientFactory)
        {
            return httpClientFactory.CreateClient(Constants.ASSEMBLY_API);
        }

        public static HttpContent GetHttpContent(string value)
        {
            return new StringContent(value, Encoding.UTF8, Constants.FORMAT_APP_JSON);
        }

        public static JArray FormatList(ApiResultModel value)
        {
            return (JArray)value.Response;
        }

        public static JObject Format(ApiResultModel value)
        {
            return (JObject)value.Response;
        }

    }
}
