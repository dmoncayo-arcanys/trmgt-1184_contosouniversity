# Contoso University #

This is a project demo for IdentityServer authentication and authorization.

### Projects ###

* ContosoUniversity (.NET Client Application)
* ContosoUniversity.API (API for Angular and .NET Client)
* ContosoUniversity.IS (IdentityServer)

### How do I get set up? ###

* Build and execute migration for ContosoUniversity.API to setup database.
* Build and execute migration for ContosoUniversity.IS to setup IdentityServer related tables.
* Run ContosoUniversity.API to populate data into database.
* Run ContosoUniversity.IS to populate IdentityServer related data into database.


### Migration for ContosoUniversity.API ###
* dotnet ef migrations add InitialCreate
* dotnet ef database update


### Migration for ContosoUniversity.IS ###
* dotnet ef migrations add InitialMigration -c AuthDbContext
* dotnet ef migrations add InitialIdentityServerPersistedGrantDbMigration -c AuthPersistedGrantDbContext -o Migrations/IdentityServer/PersistedGrantDb
* dotnet ef migrations add InitialIdentityServerConfigurationDbMigration -c AuthConfigurationDbContext -o Migrations/IdentityServer/ConfigurationDb